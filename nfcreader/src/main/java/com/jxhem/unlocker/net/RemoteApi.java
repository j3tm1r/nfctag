package com.jxhem.unlocker.net;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.HEAD;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 *
 */
public interface RemoteApi {

    /*
    * curl 'https://api.getkisi.com/locks/5124/access' -H 'Authorization: KISI-LINK
    * 75388d1d1ff0dff6b7b04a7d5162cc6c' -X POST --verbose
    * */

    String BASE_URL = "https://api.getkisi.com/";

    /**
     *
     * @param lock
     * @return
     */
    @POST("locks/{lock}/access")
    @Headers("Authorization: KISI-LINK 75388d1d1ff0dff6b7b04a7d5162cc6c")
    Call<RemoteResponse> unlock(@Path("lock") String lock);
}
