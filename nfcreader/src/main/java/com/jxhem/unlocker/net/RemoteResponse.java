package com.jxhem.unlocker.net;

public class RemoteResponse {
    private String reason, code, error, message;

    public String getReason() {
        return reason;
    }

    public String getCode() {
        return code;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
