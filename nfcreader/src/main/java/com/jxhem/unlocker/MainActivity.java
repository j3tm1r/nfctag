package com.jxhem.unlocker;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.jxhem.unlocker.net.RemoteApi;
import com.jxhem.unlocker.net.RemoteResponse;
import com.jxhem.unlocker.nfc.NFCReader;
import com.jxhem.unlocker.ui.view.UnlockView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.jxhem.nfc.NFCSpec.UNLOCK_COMMAND;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static int READER_FLAGS =
            NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
    private RemoteApi remoteService;
    private final String LOG_TAG = this.getClass().getSimpleName();
    private NfcAdapter mNfcAdapter;
    private NFCReader mNFCReader;
    private UnlockView unlockView;
    private boolean appInBackground = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if (checkNFC()) {
            createApi();
        }

        unlockView = (UnlockView) findViewById(R.id.unlockView);
    }

    @Override
    protected void onStart() {
        if(appInBackground){
            startActivity(new Intent(this, SplashActivity.class));
            finish();
        }
        appInBackground = false;
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        appInBackground = true;
    }

    private boolean checkNFC() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            return false;
        }


        mNFCReader = new NFCReader(new NFCReader.Callback() {

            @Override
            public void onCommandDiscovered(String command) {
                Response<RemoteResponse> response;
                Call<RemoteResponse> responseCall;
                if (UNLOCK_COMMAND.equals(command)) {
                    responseCall = remoteService.unlock("5124");
                    responseCall.enqueue(new Callback<RemoteResponse>() {
                        @Override
                        public void onResponse(Call<RemoteResponse> call, Response<RemoteResponse> response) {
                            Log.d(LOG_TAG, "Remote api server response " + response.code());
                        }

                        @Override
                        public void onFailure(Call<RemoteResponse> call, Throwable t) {
                            Log.d(LOG_TAG, "Remote api failure " + t.getMessage());
                        }
                    });
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            unlockView.unlock();

                        }
                    });
                } else
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            unlockView.lock();
                        }
                    });
            }
        });
        mNfcAdapter.enableReaderMode(this, mNFCReader, READER_FLAGS, null);
        // mNfcAdapter.enableForegroundDispatch(this);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent == null)
            return;
        Log.d(LOG_TAG, "new intent :" + intent.getAction());

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMessages =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMessages != null) {
                NdefMessage[] messages = new NdefMessage[rawMessages.length];
                for (int i = 0; i < rawMessages.length; i++) {
                    messages[i] = (NdefMessage) rawMessages[i];
                }
                // Process the messages array.
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())) {
        }
    }

    private void createApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RemoteApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        remoteService = retrofit.create(RemoteApi.class);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
