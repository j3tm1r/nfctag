package com.jxhem.unlocker.ui.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;

import com.jxhem.unlocker.R;

public class UnlockView extends View {

    private final int mBackgroundColor;
    private final int mOpenDegrees, startDegrees, endDegrees;
    private final boolean mHiddenFirst;
    private final float mWidthRatio;
    private final int mAnimationDuration;
    private final ObjectAnimator lockAnimator;
    private final ObjectAnimator unclockAnimator;
    private Paint mLockColor, mLockBackgroundColor;
    private RectF arcRect, outerRect, centerRect, arcInnerRect;
    private Path upperHandle;
    private int degrees = 0;

    public UnlockView(Context context) {
        this(context, null, 0);
    }

    public UnlockView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UnlockView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mLockColor = new Paint();
        mLockColor.setColor(getResources().getColor(R.color.lockColor));
        mLockColor.setStyle(Paint.Style.STROKE);
        mLockColor.setStrokeWidth(16);
        mLockColor.setFlags(Paint.ANTI_ALIAS_FLAG);

        mLockBackgroundColor = new Paint();
        mLockBackgroundColor.setStyle(Paint.Style.FILL);
        mLockBackgroundColor.setColor(getResources().getColor(R.color.unlockBackgrund));
        mLockBackgroundColor.setFlags(Paint.ANTI_ALIAS_FLAG);

        mBackgroundColor = getResources().getColor(R.color.unlockBackgrund);

        upperHandle = new Path();

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.UnlockView,
                defStyleAttr,
                0
        );

        try {
            mHiddenFirst = a.getBoolean(R.styleable.UnlockView_hiddenFirst, false);
            mOpenDegrees = a.getInteger(R.styleable.UnlockView_openDegrees, 0);
            mWidthRatio = a.getFloat(R.styleable.UnlockView_widthRatio, 3 / 5);
            startDegrees = a.getInteger(R.styleable.UnlockView_startDegrees, 170);
            endDegrees = a.getInteger(R.styleable.UnlockView_endDegrees, 220);
            mAnimationDuration = a.getInteger(R.styleable.UnlockView_animationDuration, 500);
        } finally {
            a.recycle();
        }


        lockAnimator = ObjectAnimator.ofInt(this, "degrees", mOpenDegrees, 0);
        lockAnimator.setDuration(mAnimationDuration);
        lockAnimator.setStartDelay(300);
        lockAnimator.setInterpolator(new AccelerateDecelerateInterpolator());

        unclockAnimator = ObjectAnimator.ofInt(this, "degrees", 0, mOpenDegrees);
        unclockAnimator.setDuration(mAnimationDuration);
        unclockAnimator.setStartDelay(200);
        unclockAnimator.setInterpolator(new OvershootInterpolator());
        if (mHiddenFirst)
            setVisibility(View.INVISIBLE);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        canvas.drawColor(mBackgroundColor);
        int width = getWidth(), height = getHeight();
        int baseD = width < height ? width : height;
        baseD -= getPaddingLeft() + getPaddingRight();
        int rectT = getResources().getDimensionPixelSize(R.dimen.padding_icons);
        int rectWidth = (int) (rectT + baseD * mWidthRatio);
        int rectHeight = (int) (rectWidth * 0.85);

        if (arcRect == null) {
            int archW = rectWidth / 5 * 4;
            int archH = archW;

            arcRect = new RectF();
            arcRect.set(-archW / 2, -archH / 2 - rectHeight / 2, archW / 2, archH / 2 - rectHeight / 2);

            archW = rectWidth / 2;
            archH = archW;
            arcInnerRect = new RectF();
            arcInnerRect.set(-archW / 2, -archH / 2 - rectHeight / 2, archW / 2, archH / 2 - rectHeight / 2);

            mLockColor.setStrokeWidth(8);
            outerRect = new RectF(-rectWidth / 2, -rectHeight / 2, rectWidth / 2, rectHeight / 2);
            centerRect = new RectF(-rectT * 2, -rectT, rectT * 2, rectT);
        }

        canvas.translate(width / 2, height / 2 + rectHeight / 4);

        canvas.rotate(degrees / 2, arcRect.right, arcInnerRect.centerY());

        upperHandle.reset();
        upperHandle.arcTo(arcInnerRect, startDegrees, endDegrees);
        upperHandle.lineTo(arcRect.right, arcRect.centerY());
        upperHandle.arcTo(arcRect, 0, -360 + startDegrees);
        upperHandle.close();

        canvas.drawPath(upperHandle, mLockColor);

        canvas.rotate(-degrees / 2, arcRect.right, arcInnerRect.centerY());

        canvas.drawRoundRect(outerRect, rectT, rectT, mLockBackgroundColor);
        canvas.drawRoundRect(outerRect, rectT, rectT, mLockColor);
        canvas.drawRoundRect(centerRect, rectT, rectT, mLockColor);

        canvas.drawCircle(
                centerRect.centerX() + degrees / mOpenDegrees * 2 * rectT - rectT,
                centerRect.centerY(),
                rectT, mLockColor
        );

        int alpha = (int) (degrees / mOpenDegrees * 255);
        mLockColor.setAlpha(alpha > 255 ? 255 : alpha);
        mLockColor.setStyle(Paint.Style.FILL_AND_STROKE);

        canvas.drawCircle(
                centerRect.centerX() + degrees / mOpenDegrees * 2 * rectT - rectT,
                centerRect.centerY(),
                rectT, mLockColor
        );

        mLockColor.setAlpha(255);
        mLockColor.setStyle(Paint.Style.STROKE);
    }

    public void setDegrees(int degrees) {
        this.degrees = degrees;
        invalidate();
    }

    public int getDegrees() {
        return degrees;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.v("onMeasure w", MeasureSpec.toString(widthMeasureSpec));
        Log.v("onMeasure h", MeasureSpec.toString(heightMeasureSpec));

        int desiredWidth = getResources().getDimensionPixelSize(R.dimen.icon_size) + getPaddingLeft() + getPaddingRight();
        int desiredHeight = getResources().getDimensionPixelSize(R.dimen.icon_size) + getPaddingTop() + getPaddingBottom();

        setMeasuredDimension(measureDimension(desiredWidth, widthMeasureSpec),
                measureDimension(desiredHeight, heightMeasureSpec));
    }

    private int measureDimension(int desiredSize, int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = desiredSize;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }

        if (result < desiredSize) {
            Log.e("ChartView", "The view is too small, the content might get cut");
        }
        return result;
    }

    public void lock() {
        lockAnimator.start();
    }

    public void unlock() {

        if (mHiddenFirst) {
            unclockAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    UnlockView.this.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    lock();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        }

        unclockAnimator.start();
    }
}
