package com.jxhem.unlocker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    private Runnable startMainActivity;
    private final int SECOND = 1000;
    private final int MINUTE = 60 * SECOND;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        startMainActivity = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        };

        getWindow().getDecorView().postDelayed(startMainActivity, MINUTE);
    }
}
