package com.jxhem.unlocker.nfc;

import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.util.Log;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Arrays;

import static com.jxhem.nfc.NFCSpec.BuildSelectApdu;
import static com.jxhem.nfc.NFCSpec.ByteArrayToHexString;
import static com.jxhem.nfc.NFCSpec.SAMPLE_CARD_AID;
import static com.jxhem.nfc.NFCSpec.SELECT_OK_SW;

public class NFCReader implements NfcAdapter.ReaderCallback {

    private static final String LOG_TAG = "CardReader";
    private WeakReference<Callback> mCallback;

    public interface Callback {
        void onCommandDiscovered(String command);
    }

    public NFCReader(Callback callback) {
        this.mCallback = new WeakReference<>(callback);

    }

    /**
     * Callback when a new tag is discovered by the system.
     *
     * <p>Communication with the card should take place here.
     *
     * @param tag Discovered tag
     */
    @Override
    public void onTagDiscovered(Tag tag) {
        Log.i(LOG_TAG, "New tag discovered");
        // Android's Host-based Card Emulation (HCE) feature implements the ISO-DEP (ISO 14443-4)
        // protocol.
        //
        // In order to communicate with a device using HCE, the discovered tag should be processed
        // using the IsoDep class.
        IsoDep isoDep = IsoDep.get(tag);
        if (isoDep != null) {
            try {
                // Connect to the remote NFC device
                isoDep.connect();
                // Build SELECT AID command for our loyalty card service.
                // This command tells the remote device which service we wish to communicate with.
                Log.i(LOG_TAG, "Requesting remote AID: " + SAMPLE_CARD_AID);
                byte[] command = BuildSelectApdu(SAMPLE_CARD_AID);
                // Send command to remote device
                Log.i(LOG_TAG, "Sending: " + ByteArrayToHexString(command));
                byte[] result = isoDep.transceive(command);
                // If AID is successfully selected, 0x9000 is returned as the status word (last 2
                // bytes of the result) by convention. Everything before the status word is
                // optional payload, which is used here to hold the account number.
                int resultLength = result.length;
                byte[] statusWord = {result[resultLength-2], result[resultLength-1]};
                byte[] payload = Arrays.copyOf(result, resultLength-2);
                if (Arrays.equals(SELECT_OK_SW, statusWord)) {
                    // The remote NFC device will immediately respond with its stored account number
                    String command_str = new String(payload, "UTF-8");
                    Log.i(LOG_TAG, "Received: " + command_str);
                    // Inform CardReaderFragment of received account number
                    mCallback.get().onCommandDiscovered(command_str);
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error communicating with card: " + e.toString());
            }
        }
    }


}
