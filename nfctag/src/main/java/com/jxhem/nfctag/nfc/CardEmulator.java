package com.jxhem.nfctag.nfc;

import android.content.Intent;
import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.Arrays;

import static com.jxhem.nfc.NFCSpec.ByteArrayToHexString;
import static com.jxhem.nfc.NFCSpec.ConcatArrays;
import static com.jxhem.nfc.NFCSpec.NOTHING_COMMAND;
import static com.jxhem.nfc.NFCSpec.SELECT_APDU;
import static com.jxhem.nfc.NFCSpec.SELECT_OK_SW;
import static com.jxhem.nfc.NFCSpec.UNKNOWN_CMD_SW;
import static com.jxhem.nfc.NFCSpec.UNLOCK_COMMAND;

public class CardEmulator extends HostApduService {
    private static final String LOG_TAG = "CardService";
    private String mCommand = UNLOCK_COMMAND;
    private Handler mHandler = new Handler();
    private Runnable switchCommand;
    private long mDelay = 3 * 1000;

    @Override
    public void onCreate() {
        super.onCreate();
        switchCommand = new Runnable() {
            @Override
            public void run() {
                Log.d(LOG_TAG, "Switching command");
                mCommand = (mCommand.equals(UNLOCK_COMMAND)) ? NOTHING_COMMAND : UNLOCK_COMMAND;
                mHandler.postDelayed(this, mDelay);
            }
        };

        mHandler = new Handler();
        mHandler.postDelayed(switchCommand, mDelay);
    }

    @Override
    public void onDestroy() {
        mHandler.removeCallbacks(switchCommand);
        super.onDestroy();
    }

    /**
     * This method will be called when a command APDU has been received from a remote device. A
     * response APDU can be provided directly by returning a byte-array in this method. In general
     * response APDUs must be sent as quickly as possible, given the fact that the user is likely
     * holding his device over an NFC reader when this method is called.
     * <p>
     * <p class="note">If there are multiple services that have registered for the same AIDs in
     * their meta-data entry, you will only get called if the user has explicitly selected your
     * service, either as a default or just for the next tap.
     * <p>
     * <p class="note">This method is running on the main thread of your application. If you
     * cannot return a response APDU immediately, return null and use the {@link
     * #sendResponseApdu(byte[])} method later.
     *
     * @param commandApdu The APDU that received from the remote device
     * @param bundle      A bundle containing extra data. May be null.
     * @return a byte-array containing the response APDU, or null if no response APDU can be sent
     * at this point.
     */
    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle bundle) {
        Log.i(LOG_TAG, "Received APDU: " + ByteArrayToHexString(commandApdu));
        // If the APDU matches the SELECT AID command for this service,
        // send the loyalty card account number, followed by a SELECT_OK status trailer (0x9000).
        if (Arrays.equals(SELECT_APDU, commandApdu)) {
            byte[] commandBytes = mCommand.getBytes();
            return ConcatArrays(commandBytes, SELECT_OK_SW);
        } else {
            return UNKNOWN_CMD_SW;
        }
    }

    /**
     * Called if the connection to the NFC card is lost, in order to let the application know the
     * cause for the disconnection (either a lost link, or another AID being selected by the
     * reader).
     *
     * @param reason Either DEACTIVATION_LINK_LOSS or DEACTIVATION_DESELECTED
     */
    @Override
    public void onDeactivated(int reason) {
        Log.d(LOG_TAG, "Service dactivated with reason " + reason);
        startService(new Intent(this, this.getClass()));
    }
}
